package com.misiunas.flock.game;

import com.misiunas.flock.WorldPanel;
import com.misiunas.flock.game.object.*;
import com.misiunas.flock.game.object.active.Active;
import com.misiunas.flock.game.object.active.Animal;
import com.misiunas.flock.game.object.passive.Passive;
import com.misiunas.flock.game.object.passive.Plant;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * User: Tautvydas
 * Date: 3/11/13
 */
public class WorldImpl implements World{

    private long globalTime = 0;
    private int frame = 0;

    private Dimension size;

    private Random random;

    private List<Passive> passives;
    private List<Active> actives;

    public WorldImpl() {
        size = new Dimension(1000, 1000);
        passives = new ArrayList<Passive>();
        actives = new ArrayList<Active>();

        random = new Random();
    }

    public void addRandomObjects(int count){
        int activeCount = 2 + random.nextInt(count-2);
        for (int i = 0; i < activeCount; i++) {
            actives.add(new Animal(this, random.nextInt(size.width), random.nextInt(size.height)));
        }
        for (int i = 0; i < count - activeCount; i++) {
            passives.add(new Plant(this, random.nextInt(size.width), random.nextInt(size.height)));
        }
    }

    public void iterate(){
        updateWorldTime();
        for (Passive p : passives){
            p.iterate();
        }
        for (Active a : actives){
            a.iterate();
        }
    }

    private void updateWorldTime() {
        frame++;
        if (frame >= WorldPanel.GAME_UPDATE_RATE){
            frame = 0;
            globalTime++;
        }
    }

    public long getTime(){
        return globalTime;
    }

    public List<Drawable> getDrawables(){
        List<Drawable> drawables = new ArrayList<Drawable>();
        for (Passive passiveObject : passives){
            drawables.add(passiveObject.getDrawable());
        }
        for (Active activeObject : actives){
            drawables.add(activeObject.getDrawable());
        }
        return drawables;
    }

    @Override
    public Dimension getSize() {
        return size;
    }

    @Override
    public List<Active> getActiveWorldObjects() {
        return actives;
    }

    @Override
    public List<Passive> getPassiveWorldObjects() {
        return passives;
    }

    @Override
    public List<AbstractWorldObject> getAllWorldObjects() {
        return null;
    }
}
