package com.misiunas.flock;

import com.misiunas.flock.game.Drawable;
import com.misiunas.flock.game.WorldImpl;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * User: Tautvydas
 * Date: 3/10/13
 */
public class WorldPanel extends JPanel implements Runnable{

    public final static int GAME_UPDATE_RATE = 25;
    private final static int MAX_FRAME_SKIP = 5;
    private final static int MAX_FPS = 100;

    private WorldImpl world;
    private double interpolation;

    private int fps;
    private double updateDelay;

    public WorldPanel() {
        setBackground(Color.BLACK);
        setDoubleBuffered(true);

        world = new WorldImpl();
        world.addRandomObjects(100);

        Thread thread = new Thread(this);
        thread.start();
    }

    public void paint(Graphics g){
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;

        final Dimension worldSize = world.getSize();
        final Dimension screenSize = getSize();

        final double multiX = screenSize.getWidth() / worldSize.getWidth();
        final double multiY = screenSize.getHeight() / worldSize.getHeight();

        final double interpolation = this.interpolation; //caching for this cycle

        RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHints(rh);

        final List<Drawable> drawables = world.getDrawables();
        for (Drawable drawable : drawables){
            drawable.drawOn(g2d, multiX, multiY, interpolation);
        }

        final Color color = g2d.getColor();
        g2d.setColor(Color.WHITE);
        g2d.setFont(new Font("Purisa", Font.PLAIN, 13));
        g2d.drawString("FPS: " + fps, 20, 30);
        g2d.drawString("World time: " + world.getTime(), 20, 50);
        g2d.drawString("Update takes: " + updateDelay + "ms", 20, 70);

        Toolkit.getDefaultToolkit().sync(); //for linux to look smooth
        g.dispose();
    }

    @Override
    public void run() {
        final int updateDelay = 1000 / GAME_UPDATE_RATE;
        final int minDisplayDelay = 1000 / MAX_FPS;
        final int updateFpsStatsEvery = 1000;

        long nextUpdateTime = System.currentTimeMillis();
        long lastDisplayGameTime = System.currentTimeMillis();

        long lastStatsUpdateTime = lastDisplayGameTime;
        int fps = 0;

        int loops;
        double interpolation;

        //statistics
        long beforeUpdate;
        long updateSum = 0;
        int updateCount = 0;

        while (true) {

            loops = 0;
            while (System.currentTimeMillis() > nextUpdateTime && loops < MAX_FRAME_SKIP) {

                beforeUpdate = System.currentTimeMillis();
                updateGame();
                updateSum += System.currentTimeMillis() - beforeUpdate;
                updateCount++;
                if (updateCount > 25){
                    this.updateDelay = (1.0 * updateSum / updateCount);
                    updateSum = 0;
                    updateCount = 0;
                }

                nextUpdateTime += updateDelay;
                loops++;
            }

            if (System.currentTimeMillis() - lastDisplayGameTime > minDisplayDelay) {
                interpolation = ((double) (System.currentTimeMillis() + updateDelay - nextUpdateTime)) / updateDelay;
                displayGame(interpolation);
                lastDisplayGameTime = System.currentTimeMillis();
                fps++;
            }

            if (System.currentTimeMillis() - lastStatsUpdateTime > updateFpsStatsEvery){
                lastStatsUpdateTime = System.currentTimeMillis();
                this.fps = fps;
                fps = 0;
            }

        }
    }

    private void updateGame(){
        world.iterate();
    }

    private void displayGame(double interpolation) {
        setInterpolation(interpolation);
        repaint();
    }

    public void setInterpolation(double interpolation) {
        this.interpolation = interpolation;
    }
}
