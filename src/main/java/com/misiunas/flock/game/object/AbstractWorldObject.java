package com.misiunas.flock.game.object;

import com.misiunas.flock.game.World;
import com.misiunas.flock.game.tools.Coordinate;

/**
 * User: Tautvydas
 * Date: 3/11/13
 */
public abstract class AbstractWorldObject implements DrawableObject {

    protected final World world;

    private Coordinate coordinate;

    protected AbstractWorldObject(World world, double x, double y) {
        this.world = world;
        coordinate = new Coordinate(x, y);
    }

    public Coordinate getCoor(){
        return coordinate;
    }

    public double getX() {
        return coordinate.getX();
    }

    public double getY() {
        return coordinate.getY();
    }

}
