package com.misiunas.flock.game.object.active;

import com.misiunas.flock.game.Drawable;
import com.misiunas.flock.game.SimpleDrawable;
import com.misiunas.flock.game.World;
import com.misiunas.flock.game.object.AbstractWorldObject;
import com.misiunas.flock.game.tools.Speed;

/**
 * User: Tautvydas
 * Date: 3/11/13
 */
public class Active extends AbstractWorldObject {

    private Speed speed;

    public Active(World world, double x, double y, double direction, double speed) {
        super(world, x, y);
        this.speed = new Speed(direction, speed, 50.0, 1.0, 0.5);
    }

    public Speed getSpeed(){
        return speed;
    }

    @Override
    public void iterate() {
        getCoor().moveBy(speed.getSpeedXY());
        getCoor().constrain(world.getSize());
    }

    @Override
    public Drawable getDrawable(){
        final Speed.Speed2D speedXY = speed.getSpeedXY();
        int radius = 5;
        return new SimpleDrawable(getX(), getY(), speedXY.dx, speedXY.dy, radius);
    }

}
