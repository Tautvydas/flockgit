package com.misiunas.flock.game.object;

import com.misiunas.flock.game.Drawable;

/**
 * User: Tautvydas
 * Date: 3/12/13
 */
public interface DrawableObject {

    public Drawable getDrawable();

    public void iterate();
}
