package com.misiunas.flock.game;

import java.awt.*;

/**
 * User: Tautvydas
 * Date: 3/12/13
 */
public interface Drawable {

     public void drawOn(Graphics2D graphics, double multiplicandX, double multiplicandY, double interpolation);

}
