package com.misiunas.flock.game.object.passive;

import com.misiunas.flock.game.Drawable;
import com.misiunas.flock.game.World;
import com.misiunas.flock.game.object.AbstractWorldObject;

import java.awt.*;

/**
 * User: Tautvydas
 * Date: 3/11/13
 */
public class Passive extends AbstractWorldObject implements Drawable{

    public Passive(World world, double x, double y) {
        super(world, x, y);
    }

    @Override
    public Drawable getDrawable() {
        return this;
    }

    @Override
    public void iterate() {
    }

    @Override
    public void drawOn(Graphics2D graphics, double mx, double my, double interpolation) {
        int _x = (int) (mx * getX());
        int _y = (int) (my * getY());
        int _r = 5;
        graphics.fillOval(_x, _y, _r, _r);
    }
}
