package com.misiunas.flock.game.tools;

import static java.lang.Math.*;

/**
 * User: Tautvydas
 * Date: 3/14/13
 */
public class Speed {

    private double direction;
    private double speed, maxSpeed;
    private double acceleration, radialAcceleration;

    private boolean changingDirection = false;
    private boolean changingSpeed = false;

    private double targetSpeed;
    private double targetDirection;

    public Speed(double direction, double speed, double maxSpeed, double acceleration, double radialAcceleration) {
        this.direction = direction;
        this.speed = speed;
        this.maxSpeed = maxSpeed;
        this.acceleration = acceleration;
        this.radialAcceleration = radialAcceleration;
    }

    public boolean iterate(){
        if (changingSpeed){
            accelerate();
        }
        if (changingDirection){
            turn();
        }
        return isChanging();
    }

    public double getDirection() {
        return direction;
    }

    @Deprecated
    public void setDirection(double direction){
        this.direction = direction % (2* PI);
    }

    public boolean turnTo(double direction){
        changingDirection = true;
        targetDirection = direction % (2* PI);
        turn();
        return isChanging();
    }

    public boolean turnBy(double diffRadians){
        return turnTo(direction + diffRadians);
    }

    private void turn(){
        double diff = targetDirection - direction;
        if (abs(diff) <= radialAcceleration){
            direction = targetDirection;
            changingDirection = false;
        } else {
            if (direction < PI){
                if (direction < targetDirection && targetDirection < direction + PI){
                    //turn right
                    direction += radialAcceleration;
                } else {
                    //turn left
                    direction -= radialAcceleration;
                }
            } else {
                if (direction > targetDirection && targetDirection > direction - PI) {
                    //turn left
                    direction -= radialAcceleration;
                } else {
                    //turn right
                    direction += radialAcceleration;
                }
            }
        }
        direction = direction % (2*PI);
    }

    public boolean accelerateTo(double speed){
        changingSpeed = true;
        if (speed > maxSpeed){
            targetSpeed = maxSpeed;
        } else if (speed < -maxSpeed){
            targetSpeed = -maxSpeed;
        } else {
            targetSpeed = speed;
        }
        accelerate();
        return isChanging();
    }

    public boolean accelerateBy(double diffSpeed){
        return accelerateTo(speed + diffSpeed);
    }

    private void accelerate(){
        double diff = targetSpeed - speed;
        if (abs(diff) <= acceleration){
            speed = targetSpeed;
            changingSpeed = false;
        } else {
            speed += acceleration * signum(diff);
        }
    }

    public double getSpeed(){
        return speed;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public double getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(double acceleration) {
        this.acceleration = acceleration;
    }

    public double getRadialAcceleration() {
        return radialAcceleration;
    }

    public void setRadialAcceleration(double radialAcceleration) {
        this.radialAcceleration = radialAcceleration;
    }

    public boolean isChanging() {
        return changingDirection || changingSpeed;
    }

    public Speed2D getSpeedXY(){
        double dx = speed * sin(direction);
        double dy = speed * cos(direction);
        return new Speed2D(dx, dy);
    }

    public class Speed2D {
        public double dx, dy;

        public Speed2D(double dx, double dy) {
            this.dx = dx;
            this.dy = dy;
        }
    }
}
