package com.misiunas.flock.game.object.active;

import com.misiunas.flock.game.World;
import com.misiunas.flock.game.object.ClosestComparator;
import com.misiunas.flock.game.object.active.hunting.HuntBehaviour;
import com.misiunas.flock.game.object.active.hunting.Vegetarian;
import com.misiunas.flock.game.object.passive.Passive;
import com.misiunas.flock.game.object.passive.Plant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * User: Tautvydas
 * Date: 3/14/13
 */
public class Animal extends Active {

    private HuntBehaviour huntBehaviour;

    public static final double RANGE = 1.0;
    public static final double EAT_AMOUNT = 0.5;

    private double hungriness;

    private double carnivore; //probability to eat meat
    private double meatTolerance; //show how well meat is being processed
    private double cannibalism; //probability to eat its own species

    public Animal(World world, double x, double y) {
        super(world, x, y, Math.random() * 2 * Math.PI, 1.0);
        hungriness = 0.0;
        carnivore = 0.5;
        meatTolerance = 0.5;
        cannibalism = 0.1;

        huntBehaviour = new Vegetarian(world, this);
    }

    @Override
    public void iterate() {
        findNearestFoodSource();

        super.iterate();
    }

    private void findNearestFoodSource() {
        final List<Active> actives = world.getActiveWorldObjects();
        final List<Passive> passives = world.getPassiveWorldObjects();

        final ClosestComparator closestComparator = new ClosestComparator(this);

        final double goForMeat = Math.random();
        if (goForMeat > carnivore) {
            //casting hack
            final Plant target = (Plant) huntBehaviour.selectTarget();
            if (target == null){
                //wonder randomly
                return;
            }

            final double distance = this.getCoor().distance(target.getCoor());
            if (distance < RANGE) {
                double foodEaten = target.getEaten(EAT_AMOUNT);
                hungriness += foodEaten;
            } else {
                getSpeed().accelerateTo(1.0);
                getSpeed().turnTo(getCoor().angleTo(target.getCoor()));
            }
        } else {
            List<Active> sortedList = new ArrayList<Active>(actives);
            Collections.sort(sortedList, closestComparator);
        }
    }
}
