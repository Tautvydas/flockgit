package com.misiunas.flock;

import javax.swing.*;

public class FlockGame extends JFrame {

    public FlockGame() {
        add(new WorldPanel());
        setTitle("Flock game");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(800, 640);
        setLocationRelativeTo(null);
        setVisible(true);
        setResizable(true);
    }

    public static void main(String[] args) {
        new FlockGame();

    }
}
