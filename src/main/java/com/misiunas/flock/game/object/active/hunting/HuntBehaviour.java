package com.misiunas.flock.game.object.active.hunting;

import com.misiunas.flock.game.World;
import com.misiunas.flock.game.object.AbstractWorldObject;
import com.misiunas.flock.game.object.active.Animal;

/**
 * User: Tautvydas
 * Date: 4/3/13
 */
public abstract class HuntBehaviour {

    protected final World world;
    protected final Animal animal;

    protected HuntBehaviour(World world, Animal animal) {
        this.world = world;
        this.animal = animal;
    }

    public abstract AbstractWorldObject selectTarget();

}
