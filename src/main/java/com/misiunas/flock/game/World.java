package com.misiunas.flock.game;

import com.misiunas.flock.game.object.AbstractWorldObject;
import com.misiunas.flock.game.object.active.Active;
import com.misiunas.flock.game.object.passive.Passive;

import java.awt.*;
import java.util.List;

/**
 * User: Tautvydas
 * Date: 3/18/13
 */
public interface World {

    List<Active> getActiveWorldObjects();
    List<Passive> getPassiveWorldObjects();
    List<AbstractWorldObject> getAllWorldObjects();

    Dimension getSize();
}
