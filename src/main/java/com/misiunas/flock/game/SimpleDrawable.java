package com.misiunas.flock.game;

import java.awt.*;

/**
 * User: Tautvydas
 * Date: 3/12/13
 */
public class SimpleDrawable implements Drawable {

    private double x, y, dx, dy, r;

    public SimpleDrawable(double x, double y, double dx, double dy, double r) {
        this.x = x;
        this.y = y;
        this.r = r;
        this.dx = dx;
        this.dy = dy;
    }

    @Override
    public void drawOn(Graphics2D graphics, double mx, double my, double interpolation) {
        graphics.setColor(Color.DARK_GRAY);
        int _x = (int) (mx * (x + dx*interpolation));
        int _y = (int) (my * (y + dy*interpolation));
        int _r = (int) r;
        graphics.fillOval(_x, _y, _r, _r);
    }
}
