package com.misiunas.flock.game.object.active.hunting;

import com.misiunas.flock.game.World;
import com.misiunas.flock.game.object.ClosestComparator;
import com.misiunas.flock.game.object.active.Animal;
import com.misiunas.flock.game.object.passive.Passive;
import com.misiunas.flock.game.object.passive.Plant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * User: Tautvydas
 * Date: 4/3/13
 */
public class Vegetarian extends HuntBehaviour {

    public static final double RANGE = 1.0;
    public static final double EAT_AMOUNT = 0.5;

    private double hungriness;

    private double carnivore; //probability to eat meat
    private double meatTolerance; //show how well meat is being processed
    private double cannibalism; //probability to eat its own species

    public Vegetarian(World world, Animal animal) {
        super(world, animal);
    }

    @Override
    public Passive selectTarget() {
        final List<Passive> passives = world.getPassiveWorldObjects();

        final ClosestComparator closestComparator = new ClosestComparator(animal);
        List<Passive> sortedList = new ArrayList<Passive>(passives);
        Collections.sort(sortedList, closestComparator);
        for (Passive p : sortedList) {
            if (p instanceof Plant) {
                Plant plant = (Plant) p;
                if (plant.getVegetation() < EAT_AMOUNT) {
                    continue;
                }
                return plant;
            }
        }
        return null;
    }
}
