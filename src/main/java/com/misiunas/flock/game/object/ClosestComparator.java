package com.misiunas.flock.game.object;

import java.util.Comparator;

/**
 * User: Tautvydas
 * Date: 4/1/13
 */

public class ClosestComparator implements Comparator<AbstractWorldObject> {

    private final AbstractWorldObject target;

    public ClosestComparator(AbstractWorldObject target) {
        this.target = target;
    }

    @Override
    public int compare(AbstractWorldObject o1, AbstractWorldObject o2) {
        //this object goes to the back
        if (o1.equals(target)){
            return +1;
        }
        if (o2.equals(target)){
            return -1;
        }
        final double diffX1 = o1.getX() - target.getX();
        final double diffY1 = o1.getY() - target.getY();
        final double diffX2 = o2.getX() - target.getX();
        final double diffY2 = o2.getY() - target.getY();
        final double to1 = diffX1*diffX1 + diffY1*diffY1;
        final double to2 = diffX2*diffX2 + diffY2*diffY2;
        return (int) (to1 - to2);
    }
}
