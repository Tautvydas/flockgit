package com.misiunas.flock.game.tools;

import java.awt.*;

/**
 * User: Tautvydas
 * Date: 3/14/13
 */
public class Coordinate {

    private double x, y;

    public Coordinate(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void moveBy(Speed.Speed2D speed){
        x += speed.dx;
        y += speed.dy;
    }

    public void constrain(double minX, double maxX, double minY, double maxY){
        if (x < minX){
            x = minX;
        } else if (x > maxX){
            x = maxX;
        }
        if (y < minY){
            y = minY;
        } else if (y > maxY){
            y = maxY;
        }
    }

    public void constrain(Dimension dimension){
        this.constrain(0, dimension.getWidth(), 0, dimension.getHeight());
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double distance(Coordinate other){
        final double dx = other.getX() - x;
        final double dy = other.getY() - y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    public double angleTo(Coordinate target){
        return Math.atan2(target.x - x, target.y - y);
    }
}
