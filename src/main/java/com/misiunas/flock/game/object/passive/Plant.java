package com.misiunas.flock.game.object.passive;

import com.misiunas.flock.game.World;

import java.awt.*;

/**
 * User: Tautvydas
 * Date: 3/14/13
 */
public class Plant extends Passive {

    private double vegetation, maxVegetation;
    private double growthRate;

    public Plant(World world, double x, double y) {
        this(world, x, y, 0.0005);
    }

    public Plant(World world, double x, double y, double growthRate) {
        super(world, x, y);
        vegetation = 0.1;
        this.growthRate = growthRate;
        maxVegetation = 1.0;
    }

    @Override
    public void iterate() {
        vegetation += growthRate;
        if (vegetation > maxVegetation){
            vegetation = maxVegetation;
        }
    }

    public double getVegetation() {
        return vegetation;
    }

    public double getGrowthRate() {
        return growthRate;
    }

    public double getEaten(double amount){
        if (vegetation > amount){
            vegetation -= amount;
            return amount;
        } else {
            double actualAmount = vegetation;
            vegetation = 0;
            return actualAmount;
        }
    }

    @Override
    public void drawOn(Graphics2D graphics, double mx, double my, double interpolation) {
        int green = (int) (75 + 125 * vegetation / maxVegetation);
        Color color = new Color(75, green, 75);
        graphics.setColor(color);
        int _x = (int) (mx * getX());
        int _y = (int) (my * getY());
        int _r = (int) (1 + 4 * vegetation / maxVegetation);
        graphics.fillOval(_x, _y, _r, _r);
    }
}
